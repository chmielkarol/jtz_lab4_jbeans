package ziarenka;

/**
   @version 1.20 2001-08-21
   @author Cay Horstmann
*/

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.beans.*;
import java.util.StringTokenizer;

/**
 * Edytor tabeli liczb zmiennoprzecinkowych.
 */
public class DoubleArrayEditor extends PropertyEditorSupport {
	public Component getCustomEditor() {
		return new DoubleArrayEditorPanel(this);
	}

	public boolean supportsCustomEditor() {
		return true;
	}

	public String getJavaInitializationString() {
		Double[] values = (Double[]) getValue();

		StringBuffer s = new StringBuffer();
		s.append("new Double[]{");
		for (int i = 0; i < values.length - 1; i++) {
			s.append(values[i]);
			s.append(",");
		}
		s.append(values[values.length - 1]);
		s.append("}");
		return s.toString();
	}

	public boolean isPaintable() {
		return false;
	}

	public String getAsText() {
		Double[] values = (Double[]) getValue();
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < values.length - 1; i++) {
			s.append(values[i]);
			s.append(" ");
		}
		s.append(values[values.length - 1]);
		return s.toString();
	}

	public void setAsText(String s) {
		StringTokenizer tokenizer = new StringTokenizer(s);

		int i = 0;
		Double[] values = new Double[tokenizer.countTokens()];
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			try {
				System.out.println(token);
				values[i] = Double.parseDouble(token);
				i++;
			} catch (NumberFormatException exception) {
			}
		}
		setValue(values);

	}
	
}
