package ziarenka;

/**
   @version 1.10 1997-10-27
   @author Cay Horstmann
*/

import java.beans.*;

/**
 * Klasa edytora w�a�ciwo�ci titlePosition ziarnka ChartBean. Editor umo�liwia
 * wyb�r jednej z warto�ci Left, Center, lub Right
 */
public class TitlePositionEditor extends PropertyEditorSupport {
	public String getAsText() {
		int value = ((Integer) getValue()).intValue();
		return options[value];
	}

	public void setAsText(String s) {
		for (int i = 0; i < options.length; i++) {
			if (options[i].equals(s)) {
				setValue(new Integer(i));
				return;
			}
		}
	}

	public String[] getTags() {
		return options;
	}

	private String[] options = { "Left", "Center", "Right" };
}
