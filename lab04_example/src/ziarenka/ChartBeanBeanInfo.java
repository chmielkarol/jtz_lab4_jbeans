package ziarenka;

/**
  @version 1.10 1997-10-27
  @author Cay Horstmann
*/

import java.awt.Image;
import java.beans.*;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Klasa informacyjna ziarnka. Specyfikuje edytory właściwości i customizer
 */
public class ChartBeanBeanInfo extends SimpleBeanInfo {
	//public BeanDescriptor getBeanDescriptor() {
	//	return new BeanDescriptor(ChartBean.class, ChartBeanCustomizer.class);
	//}

	public Image getIcon(int iconType) {
		String name = "";
		if (iconType == BeanInfo.ICON_COLOR_16x16)
			name = "COLOR_16x16";
		else if (iconType == BeanInfo.ICON_COLOR_32x32)
			name = "COLOR_32x32";
		else if (iconType == BeanInfo.ICON_MONO_16x16)
			name = "MONO_16x16";
		else if (iconType == BeanInfo.ICON_MONO_32x32)
			name = "MONO_32x32";
		else
			return null;
		Image im = null;
		try {
			im = ImageIO.read(ChartBeanBeanInfo.class.getClassLoader().getResourceAsStream("ChartBean_" + name + ".gif"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return im;
	}

	public PropertyDescriptor[] getPropertyDescriptors() {
		try {
			//PropertyDescriptor inverseDescriptor = new PropertyDescriptor("inverse", ChartBean.class);
			//inverseDescriptor.setPropertyEditorClass(InverseEditor.class);
			PropertyDescriptor titleDescriptor = new PropertyDescriptor("title", ChartBean.class);
			PropertyDescriptor textSizeDescriptor = new PropertyDescriptor("textSize", ChartBean.class);

			return new PropertyDescriptor[] { 
					titleDescriptor,
					textSizeDescriptor
					};
		} catch (IntrospectionException e) {
			e.printStackTrace();
			return null;
		}
	}
}
