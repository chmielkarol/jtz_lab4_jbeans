package ziarenka;

/**
   @version 1.30 2001-08-21
   @author Cay Horstmann
*/

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import javax.swing.*;

/**
 * Ziarnko tworz�ce wykres s�upkowy.
 */
public class ChartBean extends JPanel {

	public ChartBean() {
		//setBackground(Color.LIGHT_GRAY);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
      //  g2.drawLine(0, 0, 100, 100);
        
		if (values == null)
			return;
		if( values.length == 0)
			return;
		double minValue = 0;
		double maxValue = 0;
		for (int i = 0; i < values.length; i++) {
			if (minValue > getValues(i))
				minValue = getValues(i);
			if (maxValue < getValues(i))
				maxValue = getValues(i);
		}
		if (maxValue == minValue)
			return;
		
		Rectangle2D bounds = getBounds();
		double clientWidth = bounds.getWidth();
		double clientHeight = bounds.getHeight();
		double barWidth = clientWidth / values.length;

		g2.setColor(inverse ? color : Color.white);
		g2.fill(bounds);
		g2.setColor(Color.black);

		Font titleFont = new Font("SansSerif", Font.BOLD, 20);
		FontRenderContext context = g2.getFontRenderContext();
		Rectangle2D titleBounds = titleFont.getStringBounds(title, context);

		double titleWidth = titleBounds.getWidth();
		double y = -titleBounds.getY();
		double x;
		if (titlePosition == LEFT)
			x = 0;
		else if (titlePosition == CENTER)
			x = (clientWidth - titleWidth) / 2;
		else
			x = clientWidth - titleWidth;

		g2.setFont(titleFont);
		g2.drawString(title, (float) x, (float) y);

		double top = titleBounds.getHeight();
		double scale = (clientHeight - top) / (maxValue - minValue);
		y = clientHeight;

		for (int i = 0; i < values.length; i++) {
			double x1 = i * barWidth + 1;
			double y1 = top;
			double value = getValues(i);
			double height = value * scale;
			if (value >= 0)
				y1 += (maxValue - value) * scale;
			else {
				y1 += (int) (maxValue * scale);
				height = -height;
			}

			g2.setColor(inverse ? Color.white : color);
			Rectangle2D bar = new Rectangle2D.Double(x1, y1, barWidth - 2, height);
			g2.fill(bar);
			g2.setColor(Color.black);
			g2.draw(bar);
			
		}
	}

	/**
	 * Metoda set w�a�ciwo�ci title.
	 * 
	 * @param t
	 *            tytu� wykresu.
	 */
	public void setTitle(String t) {
		title = t;
	}

	/**
	 * Metoda get w�a�ciwo�ci title.
	 * 
	 * @return tytu� wykresu.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Metoda set indeksowanej w�a�ciwo�ci values.
	 * 
	 * @param v
	 *            warto�ci prezentowane na wykresie.
	 */
	public void setValues(Double[] v) {
		values = new Double[v.length];
		for(int i=0; i<v.length;i++)
			values[i]=new Double(v[i]);
	}

	/**
	 * Metoda get indeksowanej w�a�ciwo�ci values.
	 * 
	 * @return warto�ci prezentowane na wykresie.
	 */
	public Double[] getValues() {
		return values;
	}

	/**
	 * Metoda set indeksowanej w�a�ciwo�ci values.
	 * 
	 * @param i
	 *            indeks warto�ci
	 * @param value
	 *            nowa warto��
	 */
	public void setValues(int i, Double value) {
		if (0 <= i && i < values.length)
			values[i] = value;
	}

	/**
	 * Metoda get indeksowanej w�a�ciwo�ci values.
	 * 
	 * @param i
	 *            indeks warto�ci
	 * @return warto��
	 */
	public Double getValues(int i) {
		if (0 <= i && i < values.length)
			return values[i];
		return 0.0;
	}

	/**
	 * Metoda set w�a�ciwo�ci inverse.
	 * 
	 * @param b
	 *            warto�c true je�li wykres ma mie� posta� bia�ych s�upk�w na
	 *            kolorowym tle
	 */
	public void setInverse(boolean b) {
		inverse = b;
	}

	/**
	 * Metoda get w�a�ciwo�ci inverse.
	 * 
	 * @return warto�� true je�li wykres ma posta� bia�ych s�upk�w na kolorowym
	 *         tle
	 */
	public boolean isInverse() {
		return inverse;
	}

	/**
	 * Metoda set w�a�ciwo�ci titlePosition.
	 * 
	 * @param p
	 *            jedna z warto�ci LEFT, CENTER, lub RIGHT
	 */
	public void setTitlePosition(int p) {
		titlePosition = p;
	}

	/**
	 * Metoda get w�a�ciwo�ci titlePosition.
	 * 
	 * @return jedna z warto�ci LEFT, CENTER, lub RIGHT
	 */
	public int getTitlePosition() {
		return titlePosition;
	}

	/**
	 * Metoda set w�a�ciwo�ci graphColor.
	 * 
	 * @param c
	 *            kolor s�upk�w wykresu
	 */
	public void setGraphColor(Color c) {
		color = c;
	}

	/**
	 * Metoda get w�a�ciwo�ci graphColor.
	 * 
	 * @param c
	 *            kolor s�upk�w wykresu
	 */
	public Color getGraphColor() {
		return color;
	}

	public Dimension getPreferredSize() {
		return new Dimension(XPREFSIZE, YPREFSIZE);
	}

	private static final int LEFT = 0;
	private static final int CENTER = 1;
	private static final int RIGHT = 2;

	private static final int XPREFSIZE = 300;
	private static final int YPREFSIZE = 300;
	private Double[] values = { 1.0, 2.0, 3.0 };
	private String title = "Title";
	private int titlePosition = CENTER;
	private boolean inverse;
	private Color color = Color.red;
}
