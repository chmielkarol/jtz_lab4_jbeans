package ziarenka;

/**
   @version 1.10 1999-09-29
   @author Cay Horstmann
*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * Klasa indywidualizacji ziarnka wykresu umo�liwiaj�ca edycj� wszystkich jego
 * w�a�ciwo�ci w jednym oknie dialogowym z zak�adkami.
 */
public class ChartBeanCustomizer extends JTabbedPane implements Customizer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChartBeanCustomizer() {
		data = new JTextArea();
		JPanel dataPane = new JPanel();
		dataPane.setLayout(new BorderLayout());
		dataPane.add(new JScrollPane(data), BorderLayout.CENTER);
		JButton dataButton = new JButton("Set data");
		dataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				setData(data.getText());
			}
		});
		JPanel p = new JPanel();
		p.add(dataButton);
		dataPane.add(p, BorderLayout.SOUTH);

		JPanel colorPane = new JPanel();
		colorPane.setLayout(new BorderLayout());

		normal = new JCheckBox("Normal", true);
		inverse = new JCheckBox("Inverse", false);
		p = new JPanel();
		p.add(normal);
		p.add(inverse);
		ButtonGroup g = new ButtonGroup();
		g.add(normal);
		g.add(inverse);
		normal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				setInverse(false);
			}
		});

		inverse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				setInverse(true);
			}
		});

		colorEditor = PropertyEditorManager.findEditor(Color.class);
		colorEditor.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				setGraphColor((Color) colorEditor.getValue());
			}
		});

		colorPane.add(p, BorderLayout.NORTH);
		colorPane.add(colorEditor.getCustomEditor(), BorderLayout.CENTER);

		JPanel titlePane = new JPanel();
		titlePane.setLayout(new BorderLayout());

		g = new ButtonGroup();
		position = new JCheckBox[3];
		position[0] = new JCheckBox("Left", false);
		position[1] = new JCheckBox("Center", true);
		position[2] = new JCheckBox("Right", false);

		p = new JPanel();
		for (int i = 0; i < position.length; i++) {
			final int value = i;
			p.add(position[i]);
			g.add(position[i]);
			position[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					setTitlePosition(value);
				}
			});
		}

		titleField = new JTextField();
		titleField.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent evt) {
				setTitle(titleField.getText());
			}

			public void insertUpdate(DocumentEvent evt) {
				setTitle(titleField.getText());
			}

			public void removeUpdate(DocumentEvent evt) {
				setTitle(titleField.getText());
			}
		});

		titlePane.add(titleField, BorderLayout.NORTH);
		titlePane.add(p, BorderLayout.SOUTH);
		addTab("Color", colorPane);
		addTab("Title", titlePane);
		addTab("Data", dataPane);

		// obej�cie b��du komponentu JTabbedPane w JDK 1.2
		addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				validate();
			}
		});
	}

	/**
	 * Okre�la dane prezentowane na wykresie.
	 * 
	 * @param s
	 *            �a�cuch znak�w zawieraj�cy sekwencj� danych oddzielonych
	 *            znakiem spacji
	 */
	public void setData(String s) {
		StringTokenizer tokenizer = new StringTokenizer(s);

		int i = 0;
		Double[] values = new Double[tokenizer.countTokens()];
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			try {
				System.out.println(token);
				values[i] = Double.parseDouble(token);
				i++;
			} catch (NumberFormatException exception) {
			}
		}
		setValues(values);
	}

	/**
	 * Okre�la tytu� wykresu.
	 * 
	 * @param newValue
	 *            nowy tytu�
	 */
	public void setTitle(String newValue) {
		if (bean == null)
			return;
		String oldValue = bean.getTitle();
		bean.setTitle(newValue);
		firePropertyChange("title", oldValue, newValue);
	}

	/**
	 * Okre�la po�o�enie tytu�u wykresu.
	 * 
	 * @param i
	 *            nowe po�o�enie tytu�u (ChartBean.LEFT, ChartBean.CENTER lub
	 *            ChartBean.RIGHT)
	 */
	public void setTitlePosition(int i) {
		if (bean == null)
			return;
		Integer oldValue = new Integer(bean.getTitlePosition());
		Integer newValue = new Integer(i);
		bean.setTitlePosition(i);
		firePropertyChange("titlePosition", oldValue, newValue);
	}

	/**
	 * Okre�la warto�� w�a�ciwo�ci inverse.
	 * 
	 * @param b
	 *            warto�� true je�li kolory wykresu i t�a maj� by� zamienione
	 */
	public void setInverse(boolean b) {
		if (bean == null)
			return;
		Boolean oldValue = new Boolean(bean.isInverse());
		Boolean newValue = new Boolean(b);
		bean.setInverse(b);
		firePropertyChange("inverse", oldValue, newValue);
	}

	/**
	 * Okre�la warto�ci prezentowane na wykresie.
	 * 
	 * @param newValue
	 *            tablica nowych warto�ci
	 */
	public void setValues(Double[] newValue) {
		if (bean == null)
			return;
		Double[] oldValue = bean.getValues();
		bean.setValues(newValue);
		firePropertyChange("values", oldValue, newValue);
	}

	/**
	 * Okre�la kolor wykresu.
	 * 
	 * @param newValue
	 *            nowy kolor
	 */
	public void setGraphColor(Color newValue) {
		if (bean == null)
			return;
		Color oldValue = bean.getGraphColor();
		bean.setGraphColor(newValue);
		firePropertyChange("graphColor", oldValue, newValue);
	}

	public void setObject(Object obj) {
		bean = (ChartBean) obj;

		data.setText("");
		Double[] values = bean.getValues();
		for (int i = 0; i < values.length; i++)
			data.append(values[i] + "\n");

		normal.setSelected(!bean.isInverse());
		inverse.setSelected(bean.isInverse());

		titleField.setText(bean.getTitle());

		for (int i = 0; i < position.length; i++)
			position[i].setSelected(i == bean.getTitlePosition());

		colorEditor.setValue(bean.getGraphColor());
	}

	public Dimension getPreferredSize() {
		return new Dimension(XPREFSIZE, YPREFSIZE);
	}

	private static final int XPREFSIZE = 200;
	private static final int YPREFSIZE = 120;
	private ChartBean bean;
	private PropertyEditor colorEditor;

	private JTextArea data;
	private JCheckBox normal;
	private JCheckBox inverse;
	private JCheckBox[] position;
	private JTextField titleField;
}
