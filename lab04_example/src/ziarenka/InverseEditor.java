package ziarenka;

/**
   @version 1.20 2001-08-21
   @author Cay Horstmann
*/

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.beans.*;

/**
 * Edytor w�a�ciwo�ci inverse ziarnka ChartBean. W�a�ciwo�� inverse umo�liwia
 * wyb�r kolorowych s�upk�w wykresu lub kolorowego t�a.
 */
public class InverseEditor extends PropertyEditorSupport {
	public Component getCustomEditor() {
		return new InverseEditorPanel(this);
	}

	public boolean supportsCustomEditor() {
		return true;
	}

	public boolean isPaintable() {
		return true;
	}

	public String getJavaInitializationString() {
		return ((Boolean) getValue()).booleanValue() ? "true" : "false";
	}

	public void paintValue(Graphics g, Rectangle box) {
		Graphics2D g2 = (Graphics2D) g;
		boolean isInverse = ((Boolean) getValue()).booleanValue();
		String s = isInverse ? "Inverse" : "Normal";
		g2.setColor(isInverse ? Color.black : Color.white);
		g2.fill(box);
		g2.setColor(isInverse ? Color.white : Color.black);
		FontRenderContext context = g2.getFontRenderContext();
		Rectangle2D stringBounds = g2.getFont().getStringBounds(s, context);
		double w = stringBounds.getWidth();
		double x = box.x;
		if (w < box.width)
			x += (box.width - w) / 2;
		double ascent = -stringBounds.getY();
		double y = box.y + (box.height - stringBounds.getHeight()) / 2 + ascent;
		g2.drawString(s, (float) x, (float) y);
	}

	public String getAsText() {
		return null;
	}
}
