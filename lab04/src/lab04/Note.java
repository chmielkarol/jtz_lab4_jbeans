package lab04;

import java.time.LocalDateTime;

public class Note {

	private LocalDateTime date;
	private String text;
	
	public Note(String text, LocalDateTime date) {
		this.text = text;
		this.date = date;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
}
