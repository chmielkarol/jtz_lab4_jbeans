package lab04;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.Dimension;

public class Window {

	private JFrame frmPrzykadUyciaTerminarza;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmPrzykadUyciaTerminarza.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPrzykadUyciaTerminarza = new JFrame();
		frmPrzykadUyciaTerminarza.setTitle("Przyk\u0142ad u\u017Cycia terminarza");
		frmPrzykadUyciaTerminarza.setBounds(100, 100, 476, 351);
		frmPrzykadUyciaTerminarza.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPrzykadUyciaTerminarza.getContentPane().setLayout(null);
		
		Timetable timetable = new Timetable();
		timetable.setTitle("t");
		timetable.setTextSize(new Dimension(33, 300));
		timetable.setBounds(76, 23, 335, 300);
		frmPrzykadUyciaTerminarza.getContentPane().add(timetable);
	}
}
