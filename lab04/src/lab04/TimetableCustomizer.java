package lab04;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;

public class TimetableCustomizer extends JTabbedPane implements Customizer, KeyListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	
	private Timetable bean;
	
	public TimetableCustomizer() {
		
		JPanel panel = new JPanel();
		addTab("Tytu\u0142 panelu", null, panel, null);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Ustaw tytu\u0142", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.add(panel_2);
		
		textField = new JTextField();
		panel_2.add(textField);
		textField.setColumns(20);
		textField.addKeyListener(this); 
		
		JPanel panel_1 = new JPanel();
		addTab("Rozmiar pola tekstowego", null, panel_1, null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Ustaw rozmiar", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.add(panel_3);
		
		textField_1 = new JTextField();
		panel_3.add(textField_1);
		textField_1.setColumns(5);
		textField_1.addKeyListener(this); 
		
		JLabel lblNewLabel = new JLabel("x");
		panel_3.add(lblNewLabel);
		
		textField_2 = new JTextField();
		panel_3.add(textField_2);
		textField_2.setColumns(5);
		textField_2.addKeyListener(this); 
	}
	
	public void setObject(Object obj) {
		bean = (Timetable) obj;
		textField.setText(bean.getTitle());
		int width = (int) bean.getSize().getWidth();
		textField_1.setText(String.valueOf(width));
		int height = (int) bean.getSize().getHeight();
		textField_2.setText(String.valueOf(height));
	}

	public Dimension getPreferredSize() {
		return new Dimension(XPREFSIZE, YPREFSIZE);
	}
	
	private void setTitle() {
		if (bean != null) {
			String title = textField.getText();
			bean.setTitle(title);
		}
	}
	
	private void setTextSize() {
		if (bean != null) {
			int x = Integer.valueOf(textField_1.getText());
			int y = Integer.valueOf(textField_2.getText());
			bean.setTextSize(new Dimension(x, y));
		}
	}

	private static final int XPREFSIZE = 300;
	private static final int YPREFSIZE = 120;

	@Override
	public void keyPressed(KeyEvent arg0) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		setTitle();
		setTextSize();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}
}
