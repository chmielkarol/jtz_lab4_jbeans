package lab04;

/**
   @version 1.30 2001-08-21
   @author Cay Horstmann
*/

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.time.LocalDateTime;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.TitledBorder;

public class Timetable extends JPanel {
	
	private ArrayList<Note> notes = new ArrayList<Note>();
	private int current = 0;

	private PropertyChangeSupport changes = new PropertyChangeSupport(this);
	private VetoableChangeSupport vetoes = new VetoableChangeSupport (this); 
	
	private JButton btnNew, btnRemove, btnPrev, btnNext;
	private JTextArea textArea;
	private JTextField textField;
	
	private static final int XPREFSIZE = 300;
	private static final int YPREFSIZE = 300;
	private String title = "Title";
	private TitledBorder border = new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Terminarz", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0));
	
	private LocalDateTime begin, end;

	public Timetable() {
		addInitialNote();

		setBorder(border);
		setTitle("Terminarz");
		
		setLayout(null);
		
		btnNew = new JButton("Nowa notatka");
		btnNew.setBounds(50, 215, 140, 23);
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String status = btnNew.getText();
				if(status.equals("Nowa notatka")) {
					textArea.setEditable(true);
					textArea.setText("");
					
					textField.setEditable(true);
					textField.setText("");
					
					btnRemove.setEnabled(false);
					btnNew.setText("Dodaj");
				}
				else {
					String text = textArea.getText();
					String str = textField.getText();
					LocalDateTime date = LocalDateTime.parse(str);
					Note note = new Note(text, date);
					
					boolean added = false;
					for(int i = 0; i < notes.size(); ++i) {
						Note n = notes.get(i);
						if(note.getDate().isBefore(n.getDate())) {
							notes.add(i, note);
							added = true;
							break;
						}
					}
					if(!added) {
						notes.add(note);
					}
					
					btnNew.setText("Nowa notatka");
					
					showNote(current);
				}
			}
		});
		add(btnNew);
		
		btnRemove = new JButton("Usu� notatk�");
		btnRemove.setBounds(200, 215, 106, 23);
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				notes.remove(current);
				if(current!=0) --current;
				showNote(current);
			}
		});
		add(btnRemove);
		
		textArea = new JTextArea();
		textArea.setBounds(35, 53, 283, 151);
		add(textArea);
		
		textField = new JTextField();
		textField.setBounds(113, 20, 106, 20);
		textField.setColumns(10);
		textField.setEditable(false);
		add(textField);
		
		btnPrev = new JButton("\u25C4");
		btnPrev.setBounds(46, 19, 57, 23);
		btnPrev.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				--current;
				showNote(current);
			}
		});
		add(btnPrev);
		
		btnNext = new JButton("\u25BA");
		btnNext.setBounds(229, 19, 57, 23);
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				++current;
				showNote(current);
			}
		});
		add(btnNext);
		
		showNote(current);
	}
	
	private void showNote(int index) {
		if(index!=-1) {
			Note note = notes.get(index);
			textArea.setText(note.getText());
			textField.setText(note.getDate().toString());
			textArea.setEditable(false);
			
			if(index!=0) btnPrev.setEnabled(true);
			else btnPrev.setEnabled(false);
			if(index!=(notes.size()-1)) btnNext.setEnabled(true);
			else btnNext.setEnabled(false);
			if(notes.size()==1) btnRemove.setEnabled(false);
			else btnRemove.setEnabled(true);
		}
	}
	
	private void addInitialNote() {
		Note note = new Note("To jest przyk�adowa notatka.", LocalDateTime.now());
		notes.add(note);
		current = 0;
	}
	
	private static final long serialVersionUID = 1L;

	public void setTitle(String t) {
		border.setTitle(t);
		repaint();
	}
	
	public String getTitle() {
		return title;
	}
	
	public Dimension getTextSize() {
		return textArea.getSize();
	}
	
	public void setTextSize(Dimension dimension) {
		Dimension oldTextSize = textArea.getSize();
		textArea.setSize(dimension);
		changes.firePropertyChange ("textSize", oldTextSize, dimension); 
	}
	
	public Dimension getPreferredSize() {
		return new Dimension(XPREFSIZE, YPREFSIZE);
	}
	
	public LocalDateTime getBegin() {
		return begin;
	}
	
	public void setBegin(LocalDateTime begin) throws PropertyVetoException {
		LocalDateTime old = this.begin;
		vetoes.fireVetoableChange("begin", old, begin);
		this.begin = begin;
		changes.firePropertyChange("begin", old, begin);
	}
	
	public LocalDateTime getEnd() {
		return end;
	}
	
	public void setEnd(LocalDateTime end) throws PropertyVetoException{
		LocalDateTime old = this.end;
		vetoes.fireVetoableChange("end", old, end);
		this.end = end;
		changes.firePropertyChange("end", old, end);
	}
	
	public void addPropertyChangeListener(PropertyChangeListener p) {
		changes.addPropertyChangeListener(p);
	}

	public void removePropertyChangeListener(PropertyChangeListener p) {
		changes.removePropertyChangeListener(p);
	}

	public void addVetoableChangeListener(VetoableChangeListener v) {
		vetoes.addVetoableChangeListener(v);
	}

	public void removeVetoableChangeListener(VetoableChangeListener v) {
		vetoes.removeVetoableChangeListener(v);
	}
}
