package lab04;

import java.awt.Image;
import java.beans.*;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;

/**
 * Klasa informacyjna ziarnka. Specyfikuje edytory w�a�ciwo�ci i customizer
 */
public class TimetableBeanInfo extends SimpleBeanInfo {
	
	public BeanDescriptor getBeanDescriptor() {
		return new BeanDescriptor(Timetable.class, TimetableCustomizer.class);
	}

	public Image getIcon(int iconType) {
		String name = "";
		if (iconType == BeanInfo.ICON_COLOR_16x16)
			name = "COLOR_16x16";
		else if (iconType == BeanInfo.ICON_COLOR_32x32)
			name = "COLOR_32x32";
		else if (iconType == BeanInfo.ICON_MONO_16x16)
			name = "MONO_16x16";
		else if (iconType == BeanInfo.ICON_MONO_32x32)
			name = "MONO_32x32";
		else
			return null;
		Image im = null;
		try {
			im = ImageIO.read(TimetableBeanInfo.class.getClassLoader().getResourceAsStream("ChartBean_" + name + ".gif"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return im;
	}

	public PropertyDescriptor[] getPropertyDescriptors() {
		try {
			PropertyDescriptor titleDescriptor = new PropertyDescriptor("title", Timetable.class);
			PropertyDescriptor textSizeDescriptor = new PropertyDescriptor("textSize", Timetable.class);
			PropertyDescriptor beginDescriptor = new PropertyDescriptor("begin", Timetable.class);
			PropertyDescriptor endDescriptor = new PropertyDescriptor("end", Timetable.class);
			//valuesDescriptor.setPropertyEditorClass(DoubleArrayEditor.class);

			return new PropertyDescriptor[] { 
					titleDescriptor,
					textSizeDescriptor,
					beginDescriptor,
					endDescriptor
					};
		} catch (IntrospectionException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public MethodDescriptor[] getMethodDescriptors() {
		 Method addPropertyChangeListener, removePropertyChangeListener;
		 Method addVetoableChangeListener, removeVetoableChangeListener;
		 Class<?> propertyChangeArgs[] = { java.beans.PropertyChangeListener.class };
		 Class<?> vetoableChangeArgs[] = { java.beans.VetoableChangeListener.class };

		 try {
		 addPropertyChangeListener = Timetable.class.getMethod("addPropertyChangeListener", propertyChangeArgs);
		 removePropertyChangeListener = Timetable.class.getMethod("removePropertyChangeListener", propertyChangeArgs);
		 addVetoableChangeListener = Timetable.class.getMethod("addVetoableChangeListener", vetoableChangeArgs);
		 removeVetoableChangeListener = Timetable.class.getMethod("removeVetoableChangeListener", vetoableChangeArgs);
		 } catch (Exception ex) {
		 throw new Error("Missing method: " + ex);
		 }

		 // Utworzenie tablicy MethodDescriptor z widocznymi metodami obs�ugi zdarze�
		 MethodDescriptor result[] = {
		 new MethodDescriptor(addPropertyChangeListener),
		 new MethodDescriptor(removePropertyChangeListener),
		 new MethodDescriptor(addVetoableChangeListener),
		 new MethodDescriptor(removeVetoableChangeListener)
		 };
		 
		 return result;
	 }
	
}
